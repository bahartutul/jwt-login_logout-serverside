﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Model
{
    public class LogedUser
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
