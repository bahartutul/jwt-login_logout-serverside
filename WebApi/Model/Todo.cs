﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Model
{
    public class Todo
    {
        public int todoId { get; set; }
        public string task { get; set; }
        public DateTime date { get; set; }
        public bool isChecked { get; set; }
        
        public int assignTo { get; set; }
    }
}
