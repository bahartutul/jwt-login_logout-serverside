﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Model.VM
{
    public class TaskCountByDate
    {
        public DateTime date { get; set; }
        public int count { get; set; }
        public int pendingCount { get; set; }
        //public List<bool> isChecked { get; set; }

    }
}
