﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using WebApi.Model;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private IConfiguration _config;      
        private readonly UserDbContext _context;        

        public LoginController(UserDbContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

       

        [HttpGet]
        public IActionResult work()
        {
            return null;
        }
       
        [AllowAnonymous]
        [HttpPost]
        public IActionResult LogedUser(LogedUser login)
        {
            
            IActionResult response = Unauthorized();
            //var user = AuthenticateUser(login);
            var user = _context.users.Where(x => x.username == login.username && x.password == login.password).FirstOrDefault();
            
            //HttpContext.Session.SetString(SessionKeyName, "2");
            
            
            if (user != null)
            {
                var tokenString = GenerateJSONWebToken(user);
                
                response =Ok(new { token = tokenString,userid=user.userId });
            }

            return response;
        }

       

        private string GenerateJSONWebToken(User userInfo)
        {
           
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
                claims: new[]
            {
                
                new Claim(JwtRegisteredClaimNames.Sub, userInfo.username),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            },
              expires: DateTime.UtcNow.AddMinutes(7),
              notBefore: DateTime.UtcNow,
              signingCredentials: credentials);

            

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

     

    }
    public static class SessionExtensions
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
