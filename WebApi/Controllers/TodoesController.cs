﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebApi.Model;
using WebApi.Model.VM;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoesController : ControllerBase
    {
        private readonly UserDbContext _context;
        private IConfiguration _config;
        public TodoesController(UserDbContext context,IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        // GET: api/Todoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskCountByDate>>> Gettodo()
        {
            List<TaskCountByDate> taskCountByDate = new List<TaskCountByDate>();
          var groupByDate = await _context.todo.GroupBy(x => x.date).ToListAsync();
            
                foreach (var a in groupByDate)
                {
                    var taskCount = new TaskCountByDate();
                    taskCount.date = a.Key;
                    taskCount.count = a.Count();
                    taskCountByDate.Add(taskCount);
                }
            
            return  taskCountByDate;
        }


        //[HttpGet("{Date}")]
        //public async Task<ActionResult<IEnumerable<Todo>>> GetByDate(string Date,string Month,string Year)
        //{
        //    return null;
        //}

        // GET: api/Todoes/5
        [HttpGet("{month}/{year}")]
        public async Task<ActionResult<IEnumerable<TaskCountByDate>>> GetTodo(int month, int year)
        {
            var result = new List<Todo>();
            List<TaskCountByDate> taskCountByDate = new List<TaskCountByDate>();
            try
            {
                //var searchDate = new DateTime(year, month);
                //var todo = await _context.todo.FindAsync(id);
                result = await _context.todo.Where(x => x.date.Month == month && x.date.Year==year).ToListAsync();
                
                var groupByDate =  result.GroupBy(x => x.date).ToList();

                var groupByDateUnchecked = result.Where(x => x.isChecked == false).GroupBy(x=>x.date).ToList();
                var flag = 0;
                var pCount = 0;
                foreach (var a in groupByDate)
                {
                    foreach(var b in groupByDateUnchecked)
                    {
                        if (a.Key == b.Key)
                        {
                            flag++;
                            pCount = b.Count();
                            break;
                        }
                        else
                        {
                            pCount = 0;
                            flag = 0;
                        }
                    }
                    if (flag > 0)
                    {
                        var taskCount = new TaskCountByDate();
                        taskCount.date = a.Key;
                        taskCount.count = a.Count();
                        taskCount.pendingCount = pCount;
                        taskCountByDate.Add(taskCount);
                    }
                    else
                    {
                        var taskCount = new TaskCountByDate();
                        taskCount.date = a.Key;
                        taskCount.count = a.Count();
                        taskCount.pendingCount = pCount;
                        taskCountByDate.Add(taskCount);
                    }
                  
                }
               


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return taskCountByDate;
        }

        [HttpGet("{date}/{month}/{year}")]
        public async Task<ActionResult<IEnumerable<Todo>>> GetTodo(int date,int month,int year)
        {
            var result =new  List<Todo>();
            try
            {
                var searchDate = new DateTime(year, month, date);
                //var todo = await _context.todo.FindAsync(id);
                 result = await _context.todo.Where(x=>x.date==searchDate).ToListAsync();
              
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           
            return result;
        }

        // PUT: api/Todoes/5
        [HttpPut]
        public async Task<IActionResult> PutTodo(Todo todo)
        {
            

            _context.Entry(todo).State = EntityState.Modified;
            await _context.SaveChangesAsync();


            return Ok();
        }

        
        // POST: api/Todoes
        [HttpPost]
        public async Task<ActionResult<Todo>> PostTodo(Todo t)
        {
            
            Todo todo = new Todo();
            todo.isChecked = false;        
            todo.assignTo =t.assignTo;            
            todo.task = t.task;
            todo.date = t.date;
            _context.todo.Add(todo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTodo", new { id = todo.todoId }, todo);
        }

        // DELETE: api/Todoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Todo>> DeleteTodo(int id)
        {
            var todo = await _context.todo.FindAsync(id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.todo.Remove(todo);
            await _context.SaveChangesAsync();

            return todo;
        }

        private bool TodoExists(int id)
        {
            return _context.todo.Any(e => e.todoId == id);
        }
    }
}
